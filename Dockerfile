FROM adoptopenjdk/openjdk8:jre8u252-b09-alpine

LABEL maintainer="Zhangfei <zhangfei.eason@gmail.com>"

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/' /etc/apk/repositories && \
    apk add --no-cache --update tzdata bash curl zip && \
    rm -rf /var/cache/apk/*

ARG app_name=dubbo-admin
ARG build_version="1.0.0-SNAPSHOT"
ARG release_date=""

ENV TZ="Asia/Shanghai" \
    JAVA_APP_DIR=/deployments/dubbo-admin \
    JAVA_APP_JAR=/deployments/dubbo-admin/${app_name}.jar \
    LOG_PATH=/var/log/dubbo-admin \
    APP_NAME=${app_name} \
    DUBBO_REGISTRY_ADDRESS="" \
    DUBBO_CONFIG_CENTER="" \
    DUBBO_METADATA_REPORT_ADDRESS=""

RUN mkdir -p /deployments/dubbo-admin/

WORKDIR /deployments/dubbo-admin

RUN mkdir -p ${LOG_PATH}

EXPOSE 8080

VOLUME ["/var/log/"]

CMD /deployments/run-java.sh \
    --admin.registry.address=${DUBBO_REGISTRY_ADDRESS} \
    --admin.config-center=${DUBBO_CONFIG_CENTER} \
    --admin.metadata-report.address=${DUBBO_METADATA_REPORT_ADDRESS}

HEALTHCHECK --interval=1m --timeout=3s \
  CMD curl -f http://127.0.0.1:8080/actuator/health || exit 1

# Set multiple labels at once, using line-continuation characters to break long lines
LABEL vendor=Zhangfei\ Incorporated \
      com.zhangfei.is-beta= \
      com.zhangfei.is-production="" \
      com.zhangfei.version=${build_version} \
      com.zhangfei.release-date=${release_date}

ADD https://raw.githubusercontent.com/fabric8io-images/run-java-sh/master/fish-pepper/run-java-sh/fp-files/run-java.sh /deployments/run-java.sh
RUN chmod +x /deployments/run-java.sh
COPY dubbo-admin-distribution/target/${app_name}-${build_version}.jar ./${app_name}.jar
